#!/bin/bash

CORE_COUNT=`getconf _NPROCESSORS_ONLN`

VAR1=`sysctl -a | awk '/temperature/ {print $2;}' | sed s/C// | tr '\n' '+' | sed 's/\(.*\)+/\1/'`
VAR2=`echo $VAR1 | bc`
VAR3=`echo $VAR2 / $CORE_COUNT | bc`
echo -e "{\"cpu\":"$VAR3"}"
