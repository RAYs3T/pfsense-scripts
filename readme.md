# PFSense Telegraf statistic collector helper scripts
## Idea and usecase
Since the official telegraf plugin for PFSense can log plenty of stats, some basics are missing.
This therefore should add some extra statistics that can be logged in influxDB for exmaple.
## Type of statistics collected
### Collect how many connections on the given interface are IPv6 vs. IPv4
[`ip_version_stats.sh`](ip_version_stats.sh) logs the amount of open stats for the IP version.

### Log load and use of Traffic Shaper queues
[`queue_stats.sh`](queue_stats.sh) logs stats about the Traffic Shaper queues used for QoS (not Limiters!)

## Installation
1. Make sure you've installed the Telegraf Package in PFSense (via System->Packages)
1. Place the [`ip_version_stats.sh`](ip_version_stats.sh) script somewhere on your PFSense
1. Change the the permissions so the file can be executed: `chmod 755 ip_version_stats.sh` 
1. Login to the PFSense GUI and navigate to `Services`->`Telegraf` 
1. Add this to the additional configuration section:
```telegraf
[[inputs.exec]]
  commands = ["/usr/local/bin/php /root/ip_version_stats_new.php pppoe0"]
  timeout = "5s"
  data_format = "influx"

[[inputs.exec]]
  commands = ["/usr/local/bin/php /root/queue_stats.php"]
  timeout = "5s"
  data_format = "influx"

[[inputs.exec]]
  commands = ["/usr/local/bin/bash /root/sys_temp.sh"]
  timeout = "5s"
  data_format = "json"
  name_suffix = "_cpu_temp"
```
