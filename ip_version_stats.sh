#!/bin/bash
# This helper scripts collects the amount of sessions of each IP version
# Written by Kevin Urbainczyk

# Get the interface from the arguments
interface=$1

# Get states filtered by IP type
ipv6_count=$(pfctl -s state -i ${interface} | grep -vE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" | wc -l | tr -d '\040\011\012\015')
ipv4_count=$(pfctl -s state -i ${interface} | grep -E  "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" | wc -l | tr -d '\040\011\012\015')

# Sum of both versions
both_count=$(($ipv4_count+$ipv6_count))

echo "ip_stats,interface=${interface} count_v4=${ipv4_count}i,count_v6=${ipv6_count}i,count_both=${both_count}i"
