#!/bin/bash
# This helper script collects stats about the queues.
# Written by Kevin Urbainczyk

# PFTop output formarts numbers. We need to get rid of this formatting...
function extract_abbreviated () {
    if [[ $1 =~ [0-9]+[kK] ]]; then
        # Remove the last character
        only_number=${1::-1}
        # Apply maths respective to the multiplier ...
        let "cleaned_value=$only_number * 1000"
    else
        # No / unknown suffix, use input as output
        cleaned_value=$1
    fi
    echo $cleaned_value;
}

# Ask pftop for the current queues and their stats
queues_output=$(pftop -s1 -w 300 -v queue -ab | awk '{if(NR>4)print}')

# Loop trough the output and package everything for influx ...
IFS=$'\n'
for queue in $queues_output; do
    queue_name=$(echo "$queue" | awk '{print $1}')
    queue_scheduler=$(echo "$queue" | awk '{print $2}')
    queue_priority=$(echo "$queue" | awk '{print $3}')
    queue_packets=$(echo "$queue" | awk '{print $4}')
    queue_bytes=$(echo "$queue" | awk '{print $5}')
    queue_dropped_packets=$(echo "$queue" | awk '{print $6}')
    queue_dropped_bytes=$(echo "$queue" | awk '{print $7}')
    queue_length=$(echo "$queue" | awk '{print $8}')

    echo -n "queue_stats,queue_name=${queue_name},"
    echo -n "queue_scheduler=${queue_scheduler}"
    echo -n " "
    echo -n "priority=${queue_priority}i"
    echo -n ",packets=$(extract_abbreviated $queue_packets)i"
    echo -n ",bytes=$(extract_abbreviated $queue_bytes)i"
    echo -n ",dropped_packets=$(extract_abbreviated $queue_dropped_packets)i"
    echo -n ",dropped_bytes=$(extract_abbreviated $queue_dropped_bytes)i"
    echo -n ",length=$(extract_abbreviated $queue_length)i"
    echo "";
done
