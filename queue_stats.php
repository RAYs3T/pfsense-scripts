<?php
include_once("shaper.inc");
$queueStatsMetricName = "queue_stats";

function generate_influx_line_format($interface, $queueName, $stats)
{
    global $queueStatsMetricName;
    $tags = $queueStatsMetricName . ',interface=' . $interface . ',queue_name=' . $queueName . '';
    $fields = ' ';
    $first = true;
    foreach ($stats as $key => $stat) {
        $value = is_numeric($stat) ? $stat . 'i' : '"' . $stat . '"';
        if (!$first) {
            $fields .= ',';
        }
        $fields .= $key . '=' . $value;
        $first = false;
    }

    $outputLine = $tags . $fields . "\n";
    echo $outputLine;
}

$queueStatsData = get_queue_stats();

foreach ($queueStatsData['interfacestats'] as $ifName => &$ifStats) {
    $statsOutput = array(
        'interface_name' => $ifStats,
    );
    foreach ($ifStats as $queueName => &$queueStats) {

        $outStats['bytes'] = $queueStats['bytes'];
        $outStats['pkts'] = $queueStats['pkts'];
        $outStats['pkts_dropped'] = $queueStats['droppedpkts'];
        $outStats['bytes_dropped'] = $queueStats['droppedbytes'];
        $outStats['queue_items'] = $queueStats['qlengthitems'];
        $outStats['queue_size'] = $queueStats['qlengthsize'];

        generate_influx_line_format($ifName, $queueName, $outStats);
    }

}