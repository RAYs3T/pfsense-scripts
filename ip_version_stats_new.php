<?php
require_once("interfaces.inc");

if(!isset($argv[1])){
        echo "No interface specified\n";
        exit(1);
}

$interface = $argv[1];

/**
 * Splits the port from the ip
**/
function get_ip($addr) {
        $parts = explode(":", $addr);
        if (count($parts) == 2) {
                return (trim($parts[0]));
        } else {
                /* IPv6 */
                $parts = explode("[", $addr);
                if (count($parts) == 2) {
                        return (trim($parts[0]));
                }
        }

        return ("");
}


// Build a filter to select only states we're interested in
$filter = array();
$filter[] = array("interface" => $interface);

$states = pfSense_get_pf_states($filter);

// Now that we have all the states per interface, we need to count the amounts of IPv4 vs IPv6
$amount_v4 = 0;
$amount_v6  = 0;

foreach ($states as &$entry){
        $destination_ip = get_ip($entry['dst']);
        if(filter_var($destination_ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) != false) {
                $amount_v4++;
        } else if (filter_var($destination_ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) != false){
                $amount_v6++;
        }
}

// Generate telegraf output
$metric = 'ip_stats';

echo $metric . ',' . 'interface=' . $interface . ' count_v4=' . $amount_v4 . 'i,count_v6=' . $amount_v6 . 'i,count_both=' . ($amount_v4 + $amount_v6) . 'i' . "\n";